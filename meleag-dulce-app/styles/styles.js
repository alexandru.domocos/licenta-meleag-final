import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.35,
    shadowRadius: 4.65,

    elevation: 6,
  },
});

export default styles;
